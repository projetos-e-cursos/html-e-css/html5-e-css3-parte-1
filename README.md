# HTML5 E CSS3, PARTTE 1

## Conceitos abordados:
- HTML;
- tags;
- Estrutura básica do HTML;
- Tags DOCTYPE, html, head e body;
- Tags para títtulos (h1, h2, etc.);
- Tags p, strong, em, meta, title;
- CSS;
- CSS inline, interno e externo;
- Estilo em cascata;
- Dicionários;
- Cores;
- UX (User Experience - Usabilidade);
- UI (User Interface - Design);
- Desenvolvedor Front-End;
- Identificadores de elementos e como referenciar ele no CSS;
- Altura e largura de elementos;
- Espaçamentos e margens;
- Listas (ul e ol);
- Imagens;
- Tag div;
- font-weight, text-transform, display, text-decoration, position, inline-block, transition, transform;
- Classes de elementos para usarem o mesmo CSS;
- Comportamentos __inline__ e __block__;
- Tags header, main e footer;
- Bordas;
- Utilizar códigos Unicode;
- Formularios;
- Tabelas;
- Organização do código mais semântico com as novas tags do HTML;
<<<<<<< HEAD
- Pseudo-classes;
- Background gradient;
- Opacidade de elementos;
- Sombras nos elementos;
- Media Queries;
- Meta tag Viewport;
=======

![barbearia_alura](/uploads/7933c51e91b410be6860464e140fb656/barbearia_alura.png)
>>>>>>> 9978ec3684bbd1e8397e646fbe6423e22a52d7eb
